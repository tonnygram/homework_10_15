# https://www.codewars.com/kata/5a662a02e626c54e87000123


def extra_perfect(number: int):
    return [x for x in range(1, number+1) if str(bin(x)[2:])[0] == '1' and str(bin(x)[2:])[-1] == '1']


print(extra_perfect(7))  # 1, 3, 5, 7
