# https://www.codewars.com/kata/5a55f04be6be383a50000187


def special_number(number: int):
    is_special_number = True
    for x in str(number):
        if x not in ['0', '1', '2', '3', '4', '5']:
            is_special_number = False
    return 'Special!!' if is_special_number else 'NOT!!'


print(special_number(709))
