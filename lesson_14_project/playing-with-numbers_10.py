# https://www.codewars.com/kata/5a99a03e4a6b34bb3c000124
from functools import reduce
from operator import mul


def is_prime(n):
    if n % 2 == 0:
        return n == 2
    d = 3
    while d * d <= n and n % d != 0:
        d += 2
    return d * d > n


def num_primorial(number: int):
    list_prime_numbers = list()
    i = 2
    while True:
        if len(list_prime_numbers) >= number:
            break
        if is_prime(i):
            list_prime_numbers.append(i)
        i += 1

    return reduce(mul, list_prime_numbers)


print(num_primorial(3))  # 30
print(num_primorial(5))  # 2310
