# https://www.codewars.com/kata/5a53a17bfd56cb9c14000003


def disarium_number(number: int):
    return 'Disarium !!' if sum(int(x) ** (k + 1) for k, x in enumerate(str(number))) == number else 'Not !!'


print(disarium_number(89))  # (8)1 + (9)2 = 89
