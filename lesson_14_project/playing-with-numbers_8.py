# https://www.codewars.com/kata/5a87449ab1710171300000fd


def tidy_number(number: int):
    return ''.join(sorted(str(number))) == str(number)


print(tidy_number(12))  # True
