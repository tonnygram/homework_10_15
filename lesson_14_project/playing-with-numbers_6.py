# https://www.codewars.com/kata/5a58d889880385c2f40000aa


def auto_morphic(number: int):
    return 'Automorphic' if str(number * number).endswith(str(number)) else 'Not!!'


print(auto_morphic(9))
