# Пробовал несколько раз сделать, но так и не нашел решения

# https://www.codewars.com/kata/5bb5e174528b2908930005b5/train/python
import itertools


def find_last(n, m):
    list_people = [x+1 for x in range(n)]
    delete_element = list()
    cycle_list_people = itertools.cycle(list_people)
    while len(list_people) > 1:
        i = 0
        result_element = 0
        while i < m:
            result_element = next(cycle_list_people)
            i += 1
        delete_element.append(result_element)
        index_element_for_delete = list_people.index(result_element)
        list_people.pop(index_element_for_delete)

    print('----------')
    print(list_people)


find_last(8, 3)
