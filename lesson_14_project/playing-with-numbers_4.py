# https://www.codewars.com/kata/5a54e796b3bfa8932c0000ed


def jumping_number(number: int):
    str_number = str(number)

    if len(str_number) == 1:
        return 'Jumping!!'
    else:
        list_diff = []
        for i in range(1, len(str_number)):
            list_diff.append(abs(int(str_number[i]) - int(str_number[i - 1])))
        is_one = True
        for x in list_diff:
            if x != 1:
                is_one = False
        return 'Jumping!!' if is_one else 'Not!!'


print(jumping_number(79))
