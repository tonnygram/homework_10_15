# https://www.codewars.com/kata/5a4d303f880385399b000001
from math import factorial


def is_sum_factorial(number: int):
    return 'STRONG!!!!' if sum([factorial(int(x)) for x in str(number)]) == number else 'Not Strong !!'


print(is_sum_factorial(145))
