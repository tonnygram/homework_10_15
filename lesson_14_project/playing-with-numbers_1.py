# series https://www.codewars.com/collections/playing-with-numbers

# https://www.codewars.com/kata/5a4e3782880385ba68000018


def balanced_num(number: int):
    str_numbers = str(number)
    if len(str_numbers) % 2 == 0:
        sum_elements_left = sum([int(x) for x in str_numbers[:len(str_numbers) // 2][:-1]])
        sum_elements_right = sum([int(x) for x in str_numbers[len(str_numbers) // 2:][1:]])
    else:
        index_center_element = len(str_numbers) // 2
        sum_elements_left = sum([int(x) for x in str_numbers[:index_center_element]])
        sum_elements_right = sum([int(x) for x in str_numbers[index_center_element+1:]])

    if sum_elements_left == sum_elements_right:
        return "Balanced"
    else:
        return "Not Balanced"


print(balanced_num(56239814))
