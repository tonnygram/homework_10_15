from django.test import TestCase
from django.urls import reverse
import json


class Api(TestCase):
    def test_sum(self):
        response = self.client.get(reverse('api:index'), dict(numbers='1,2,3', operator='sum'))
        response_json = json.loads(response.content.decode('utf8'))
        result_operation = response_json.get('response')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(6, result_operation)

    def test_sub(self):
        response = self.client.get(reverse('api:index'), dict(numbers='1,2,3', operator='sub'))
        response_json = json.loads(response.content.decode('utf8'))
        result_operation = response_json.get('response')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(-4, result_operation)

    def test_mul(self):
        response = self.client.get(reverse('api:index'), dict(numbers='1,2,3', operator='mul'))
        response_json = json.loads(response.content.decode('utf8'))
        result_operation = response_json.get('response')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(6, result_operation)

    def test_div(self):
        response = self.client.get(reverse('api:index'), dict(numbers='10,4,1', operator='div'))
        response_json = json.loads(response.content.decode('utf8'))
        result_operation = response_json.get('response')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(2.5, result_operation)

    def test_zero_division_error(self):
        response = self.client.get(reverse('api:index'), dict(numbers='10,4,0', operator='div'))
        response_json = json.loads(response.content.decode('utf8'))
        result_operation = response_json.get('response')
        self.assertEqual(response.status_code, 200)
        self.assertEqual('Error: division by zero!', result_operation)

    def test_mod(self):
        response = self.client.get(reverse('api:index'), dict(numbers='11,3,2', operator='mod'))
        response_json = json.loads(response.content.decode('utf8'))
        result_operation = response_json.get('response')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, result_operation)
