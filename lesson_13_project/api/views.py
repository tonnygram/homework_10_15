from functools import reduce
from operator import add, mul, truediv, sub, mod
from typing import Union

from django.http import JsonResponse, HttpRequest
from django.views.generic.base import View

__all__ = {'IndexView'}


class IndexView(View):
    def get(self, request: HttpRequest) -> JsonResponse:
        list_numbers = request.GET['numbers'].split(',')
        list_numbers = [int(x) for x in list_numbers]
        operator = request.GET['operator']

        if operator == 'sum':
            result = self._sum(list_numbers)
        elif operator == 'sub':
            result = self._sub(list_numbers)
        elif operator == 'mul':
            result = self._mul(list_numbers)
        elif operator == 'div':
            result = self._div(list_numbers)
        elif operator == 'mod':
            result = self._mod(list_numbers)
        else:
            result = None

        return JsonResponse({'response': result})

    @staticmethod
    def _sum(list_op: list) -> int:
        return reduce(lambda x, y: add(x, y), list_op)

    @staticmethod
    def _sub(list_op: list) -> int:
        return reduce(lambda x, y: sub(x, y), list_op)

    @staticmethod
    def _mul(list_op: list) -> int:
        return reduce(lambda x, y: mul(x, y), list_op)

    @staticmethod
    def _div(list_op: list) -> Union['float', 'str']:
        try:
            result_div = reduce(lambda x, y: truediv(x, y), list_op)
        except ZeroDivisionError:
            result_div = 'Error: division by zero!'
        return result_div

    @staticmethod
    def _mod(list_op: list) -> int:
        return reduce(lambda x, y: mod(x, y), list_op)
