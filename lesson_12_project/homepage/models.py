from django.db import models
import datetime

__all__ = {'File'}


class File(models.Model):
    id_file: str = models.CharField(max_length=30, null=False, blank=False, help_text='ID File', verbose_name='ID File')
    title_file: str = models.CharField(max_length=30, null=False, blank=False, help_text='Title File',
                                       verbose_name='Title File')
    upload_date: datetime.datetime = models.DateTimeField(auto_now_add=True, help_text='Date Upload',
                                                          verbose_name='Date Upload')
    content_file: str = models.TextField(help_text='Content File', verbose_name='Content File')
    result_analysis: str = models.TextField(help_text='Result Analysis', verbose_name='Result Analysis')

    def __str__(self) -> str:
        return self.id_file
