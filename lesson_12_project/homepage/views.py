import json
from collections import Counter

from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.views import generic
from django.views.generic.base import View

from homepage.models import File

__all__ = {'HomePage', 'Result', 'DetailView'}


class HomePage(View):
    @staticmethod
    def get(request: HttpRequest) -> HttpResponse:
        return render(request, 'homepage/index.html')


class Result(View):
    @staticmethod
    def get(request: HttpRequest) -> HttpResponse:
        get_last_5_files = File.objects.order_by('-upload_date')[:5]

        context = dict(last_5_files=get_last_5_files)

        return render(request, 'homepage/result.html', context)

    def post(self, request: HttpRequest) -> HttpResponse:
        id_file = request.POST['id_file']

        try:
            File.objects.get(id_file=id_file)
            messages.error(request, 'Error: this ID already exists!')
            return render(request, 'homepage/index.html')
        except ObjectDoesNotExist:
            title_file = request.POST['title_file']
            content = self._handle_uploaded_file(request.FILES['file'], title_file)
            result_analysis_text = dict(Counter(content.split()))
            content_file_in_json = json.dumps(content)

            object_file = File(id_file=id_file, title_file=title_file, content_file=content_file_in_json,
                               result_analysis=result_analysis_text)
            object_file.save()

            get_last_5_files = File.objects.order_by('-upload_date')[:5]

            context = dict(last_5_files=get_last_5_files)

            return render(request, 'homepage/result.html', context)

    @staticmethod
    def _handle_uploaded_file(f: InMemoryUploadedFile, file_name: str) -> str:
        with open(f'files/{file_name}.txt', 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
        file_json = open(f'files/{file_name}.txt')
        content_in_file = file_json.read()
        file_json.close()
        return content_in_file


class DetailView(generic.DetailView):
    model = File
    template_name = 'homepage/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        result_in_str = File.objects.get(id_file=context['file']).result_analysis
        result_in_str = result_in_str.replace('\'', '\"')
        context['result_analysis'] = json.loads(result_in_str)
        return context
