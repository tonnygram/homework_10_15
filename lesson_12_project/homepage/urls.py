from django.urls import path
from .views import HomePage, Result, DetailView

app_name = 'homepage'
urlpatterns = [
    path('', HomePage.as_view(), name='home'),
    path('result/', Result.as_view(), name='result'),
    path('result/<int:pk>/', DetailView.as_view(), name='detail'),
]
