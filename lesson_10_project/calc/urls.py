from django.urls import path

from calc.views import Index

app_name = 'calc'
urlpatterns = [
    path('', Index.as_view(), name='index'),
]
