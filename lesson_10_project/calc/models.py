from django.db import models

__all__ = {'Operator'}


class Operator(models.Model):
    operator: str = models.CharField(max_length=1, null=False, blank=False, help_text='Operator',
                                     verbose_name='Operator')

    def __str__(self) -> str:
        return self.operator
