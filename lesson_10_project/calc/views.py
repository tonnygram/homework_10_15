from django.contrib import messages
from django.shortcuts import render, redirect
from django.views import View

from calc.models import Operator


class Index(View):
    def get(self, request):
        all_operators = Operator.objects.all()
        return render(request, 'calc/index.html', {
            'operator_list': all_operators
        })

    def post(self, request):
        operand_one = request.POST.get('operand_one')
        operand_two = request.POST.get('operand_two')
        operator = request.POST.get('operator')
        if str(operand_one).isdigit() and str(operand_two).isdigit():
            if operator == '+':
                result_operation = int(operand_one) + int(operand_two)
            elif operator == '-':
                result_operation = int(operand_one) - int(operand_two)
            elif operator == '*':
                result_operation = int(operand_one) * int(operand_two)
            elif operator == '/':
                try:
                    result_operation = int(operand_one) / int(operand_two)
                except ZeroDivisionError:
                    result_operation = 'Error: division by zero!'
            else:
                result_operation = 'Error: unknown operator!'
        else:
            result_operation = 'Error: input number!'

        messages.info(request, f'Result operation: {result_operation}')
        return redirect('calc:index')
