# https://www.codewars.com/kata/5a7893ef0025e9eb50000013


def max_gap(list_numbers: list):
    list_numbers = sorted(list_numbers, reverse=True)
    diff_list = []
    for i in range(1, len(list_numbers)):
        diff_list.append(list_numbers[i] - list_numbers[i - 1])
    return max(abs(x) for x in diff_list)


# print(max_gap([13, 10, 5, 2, 9]))  # 4
print(max_gap([-3, -27, -4, -2]))  # 23
