# https://www.codewars.com/kata/5abd66a5ccfd1130b30000a9


def row_weights(array: list):
    return sum(array[::2]), sum(array[1::2])


# print(row_weights([13, 27, 49]))  # 62, 27
print(row_weights([50, 60, 70, 80]))  # 120, 140
