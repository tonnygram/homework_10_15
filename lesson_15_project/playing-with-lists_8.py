# https://www.codewars.com/kata/5ac6932b2f317b96980000ca


def min_value(digits: list):
    return int(''.join(map(str, sorted(set(digits)))))


print(min_value([1, 3, 1]))  # 13
# print(min_value([5, 7, 5, 9, 7]))  # 579
