# https://www.codewars.com/kata/5aa1bcda373c2eb596000112


def max_tri_sum(numbers: list):
    return sum(sorted(list(set(numbers)))[-3:])


# print(max_tri_sum([3, 2, 6, 8, 2, 3]))  # 17
print(max_tri_sum([2, 1, 8, 0, 6, 4, 8, 6, 2, 4]))  # 18
