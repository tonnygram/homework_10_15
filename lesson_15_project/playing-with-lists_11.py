# https://www.codewars.com/kata/5a4138acf28b82aa43000117


def adjacent_elements_product(array: list):
    return max(a*b for a, b in zip(array, array[1:]))


# print(adjacent_elements_product([1, 2, 3]))  # 6
print(adjacent_elements_product([9, 5, 10, 2, 24, -1, -48]))  # 50
