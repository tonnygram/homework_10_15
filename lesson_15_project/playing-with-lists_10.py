# https://www.codewars.com/kata/5a91a7c5fd8c061367000002


def minimum_steps(numbers: list, k: int):
    sum_elements = 0
    count_elements = 0
    sorted_iter_list = iter(sorted(numbers))
    while sum_elements < k:
        sum_elements += next(sorted_iter_list)
        count_elements += 1

    return count_elements - 1


# print(minimum_steps([1, 10, 12, 9, 2, 3], 6))  # 2
# print(minimum_steps([8, 9, 4, 2], 23))  # 3
print(minimum_steps([19, 98, 69, 28, 75, 45, 17, 98, 67], 464))  # 8
