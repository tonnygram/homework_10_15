# https://www.codewars.com/kata/5a512f6a80eba857280000fc


def nth_smallest(array: list, k: int):
    return sorted(array)[k - 1]


# print(nth_smallest([3, 1, 2], 2))  # 2
# print(nth_smallest([15, 20, 7, 10, 4, 3], 3))  # 7
print(nth_smallest([2, 169, 13, -5, 0, -1], 4))  # 2
