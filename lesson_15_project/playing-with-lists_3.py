# https://www.codewars.com/kata/5a651865fd56cb55760000e0


def array_leaders(list_numbers: list):
    return [x for k, x in enumerate(list_numbers) if x > sum(list_numbers[k+1:])]


# print(array_leaders([1, 2, 3, 4, 0]))  # 4
print(array_leaders([16, 17, 4, 3, 5, 2]))  # {17, 5, 2}
