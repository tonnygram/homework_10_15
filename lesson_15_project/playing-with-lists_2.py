# https://www.codewars.com/kata/5a63948acadebff56f000018
from functools import reduce
from operator import mul


def max_product(list_number: list, k: int):
    return reduce(mul, sorted(list_number)[-k:])


print(max_product([4, 3, 5], 2))  #20
# print(max_product([8, 10, 9, 7], 3))  #720
