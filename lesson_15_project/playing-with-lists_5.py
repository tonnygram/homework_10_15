# https://www.codewars.com/kata/5a905c2157c562994900009d
from functools import reduce
from operator import mul


def product_array(list_numbers: list):
    return [reduce(mul, list_numbers[:k] + list_numbers[k + 1:]) for k, x in enumerate(list_numbers)]


# print(product_array([1, 5, 2]))  # 10,2,5
print(product_array([10, 3, 5, 6, 2]))  # 180,600,360,300,900
