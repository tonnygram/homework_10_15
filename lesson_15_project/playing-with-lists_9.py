# https://www.codewars.com/kata/5a946d9fba1bb5135100007c
# TODO Нашел 2 решения задачи, но проверить нет возможности, поскольку время обработки более 12 секунд


def isPrime(n):
    if n % 2 == 0:
        return n == 2
    d = 3
    while d * d <= n and n % d != 0:
        d += 2
    return d * d > n


# Execution Timed Out (12000 ms)
# def minimum_number(numbers: list):
#     list_prime_numbers = [x for x in range(10000) if isPrime(x)]
#     for x in list_prime_numbers:
#         if sum(numbers) <= x:
#             return x - sum(numbers)

# Execution Timed Out (12000 ms)
def minimum_number(numbers: list):
    sum_list = sum(numbers)
    while True:
        i = sum_list + 1
        if isPrime(i):
            return i - sum_list


# print(minimum_number([3, 1, 2]))  # 1
# print(minimum_number([5, 2]))  # 0
# print(minimum_number([2, 12, 8, 4, 6]))  # 5
# print(minimum_number([50, 39, 49, 6, 17, 28]))  # 2
