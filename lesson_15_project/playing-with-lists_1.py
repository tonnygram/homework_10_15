# https://www.codewars.com/kata/5a523566b3bfa84c2e00010b
from itertools import product


def min_sum(list_number: list):
    list_product = product(list_number)
    for x in list_product:
        print(x)


print(min_sum([5, 4, 2, 3]))  # 22  5*2 + 3*4 = 22
