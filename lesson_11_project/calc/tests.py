from django.test import TestCase
from django.urls import reverse


class CalcPage(TestCase):
    def test_form_existence(self):
        response = self.client.get(reverse('calc:index'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"form", response.content)

    def test_submit_button_existence(self):
        response = self.client.get(reverse('calc:index'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"submit", response.content)

    def test_input_1_existence(self):
        response = self.client.get(reverse('calc:index'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"input_1", response.content)

    def test_input_2_existence(self):
        response = self.client.get(reverse('calc:index'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"input_2", response.content)

    def test_drop_down_existence(self):
        response = self.client.get(reverse('calc:index'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"select", response.content)


class ResultPage(TestCase):
    def test_result_existence_empty(self):
        response = self.client.post(reverse('calc:result'), dict(input_1='1', list_operators='+', input_2='2'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"1 + 2", response.content)
