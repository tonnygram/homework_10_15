from django.contrib import messages
from django.shortcuts import render
from django.views import View

from calc.models import Operation


class Index(View):
    def get(self, request):
        return render(request, 'calc/index.html')


class Result(View):
    def _calculate(self, operand_1, operator, operand_2):
        result = None
        if operator == '+':
            result = operand_1 + operand_2
        elif operator == '-':
            result = operand_1 - operand_2
        elif operator == '*':
            result = operand_1 * operand_2
        elif operator == '/':
            try:
                result = operand_1 / operand_2
            except ZeroDivisionError:
                result = 'ZeroDivisionError'
        elif operator == '**':
            result = operand_1 ** operand_2
        return result

    def post(self, request):
        operand_1 = request.POST['input_1']
        operator = request.POST['list_operators']
        operand_2 = request.POST['input_2']
        result_operation = self._calculate(int(operand_1), operator, int(operand_2))
        if result_operation == 'ZeroDivisionError':
            messages.error(request, 'Error: division by zero!')
            return render(request, 'calc/index.html')
        new_operation_object = Operation(operand_1=operand_1, operator=operator, operand_2=operand_2,
                                         result_operation=result_operation)
        new_operation_object.save()

        last_10_operation = Operation.objects.order_by('-id')[:10]
        return render(request, 'calc/result.html', context=dict(last_10_operation=last_10_operation))
