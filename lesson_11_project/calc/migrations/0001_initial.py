# Generated by Django 2.2.4 on 2020-01-28 06:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Operation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('operand_1', models.CharField(help_text='Operand_1', max_length=30, verbose_name='Operand_1')),
                ('operator', models.CharField(help_text='Operator', max_length=2, verbose_name='Operator')),
                ('operand_2', models.CharField(help_text='Operand_2', max_length=30, verbose_name='Operand_2')),
                ('result_operation', models.CharField(help_text='Result_operation', max_length=50, verbose_name='Result_operation')),
            ],
        ),
    ]
