from django.db import models
import datetime

__all__ = {'Operation'}


class Operation(models.Model):
    operand_1: str = models.CharField(max_length=30, null=False, blank=False, help_text='Operand_1',
                                      verbose_name='Operand_1')
    operator: str = models.CharField(max_length=2, null=False, blank=False, help_text='Operator',
                                     verbose_name='Operator')
    operand_2: str = models.CharField(max_length=30, null=False, blank=False, help_text='Operand_2',
                                      verbose_name='Operand_2')
    result_operation: str = models.CharField(max_length=50, null=False, blank=False, help_text='Result_operation',
                                             verbose_name='Result_operation')
    date_operation: datetime.datetime = models.DateTimeField(help_text='Date operation', verbose_name='Date operation',
                                                             auto_now_add=True)

    def __str__(self) -> str:
        return f'{__class__.__name__} ({self.operand_1}{self.operator}{self.operand_2}={self.result_operation})'
