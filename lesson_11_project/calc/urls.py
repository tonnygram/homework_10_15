from django.contrib import admin
from django.urls import path
from calc.views import Index, Result

app_name = 'calc'
urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('result/', Result.as_view(), name='result'),
]
