from django.test import TestCase
from django.urls import reverse


class RegistrationPage(TestCase):
    def test_form_existence(self):
        response = self.client.get(reverse('registration:registration'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"form", response.content)
