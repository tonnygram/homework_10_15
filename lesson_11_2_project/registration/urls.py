from django.contrib import admin
from django.urls import path, include

from registration.views import Registration, Result

app_name = 'registration'
urlpatterns = [
    path('', Registration.as_view(), name='registration'),
    path('result/', Result.as_view(), name='result'),
]
