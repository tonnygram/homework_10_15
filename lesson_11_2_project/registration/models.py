from django.db import models
import datetime

__all__ = {'Check'}


class Check(models.Model):
    product_name: str = models.CharField(max_length=30, null=False, blank=False, help_text='Product Name',
                                         verbose_name='Product Name')
    cost_product: int = models.IntegerField(null=False, blank=False, help_text='Cost Product',
                                            verbose_name='Cost Product')
    date_create_check: datetime.datetime = models.DateTimeField(help_text='Date registration check',
                                                                verbose_name='Date registration check',
                                                                auto_now_add=True)

    def __str__(self):
        return self.product_name
