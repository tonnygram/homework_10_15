from django.shortcuts import render
from django.views import View
from registration.models import Check

__all__ = {'Registration', 'Result'}


class Registration(View):
    def get(self, request):
        return render(request, 'registration/registration.html')


class Result(View):
    def post(self, request):
        product_name = request.POST['product_name']
        product_cost = request.POST['product_cost']
        object_product = Check(product_name=product_name, cost_product=int(product_cost))
        object_product.save()
        return render(request, 'registration/result.html')
