from django.test import TestCase
from django.urls import reverse


class HomePage(TestCase):
    def test_table_existence(self):
        response = self.client.get(reverse('home:index'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"table", response.content)
