from django.shortcuts import render
from django.views import View

from registration.models import Check

__all__ = {'HomePage'}


class HomePage(View):
    def get(self, request):
        all_products = Check.objects.order_by('-id')[:5]
        return render(request, 'home/index.html', dict(products=all_products))
