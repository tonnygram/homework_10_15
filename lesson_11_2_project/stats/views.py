from datetime import datetime

from django.shortcuts import render
from django.views import View

from registration.models import Check

__all__ = {'Stats'}


class Stats(View):
    def get(self, request):
        most_expensive_product_today = Check.objects.filter(date_create_check__gte=datetime.now().date()).order_by(
            'cost_product').last()
        cheapest_product_today = Check.objects.filter(date_create_check__gte=datetime.now().date()).order_by(
            '-cost_product').last()
        return render(request, 'stats/stats.html', dict(most_expensive_product_today=most_expensive_product_today,
                                                        cheapest_product_today=cheapest_product_today))
