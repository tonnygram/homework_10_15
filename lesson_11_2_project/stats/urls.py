from django.urls import path

from stats.views import Stats

app_name = 'stats'
urlpatterns = [
    path('', Stats.as_view(), name='stats'),
]
