from django.test import TestCase
from django.urls import reverse


class StatsPage(TestCase):
    def test_table_existence(self):
        response = self.client.get(reverse('stats:stats'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"table", response.content)

    def test_2_rows_in_tables_existence(self):
        response = self.client.get(reverse('stats:stats'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str(response.content).count("<tr>"), 4)
