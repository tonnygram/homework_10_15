from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('home.urls')),
    path('registration/', include('registration.urls')),
    path('stats/', include('stats.urls')),
    path('admin/', admin.site.urls),
]
